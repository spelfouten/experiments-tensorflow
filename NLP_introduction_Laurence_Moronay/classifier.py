# Based on data from
# Sarcasm Detection using Hybrid Neural Network
# R Misra, P Arora - arXiv preprint arXiv:1908.07414, 2019
# Cited by 1 Related articles All 4 versions

# Based on Training a model to recognize sentiment in text (NLP Zero to Hero)
# https://www.youtube.com/watch?v=Y_hzMnRXjhI&list=PLQY2H8rRoyvwLbzbnKJ59NkZvQAW9wLbx&index=3
# https://colab.research.google.com/github/lmoroney/dlaicourse/blob/master/TensorFlow%20In%20Practice/Course%203%20-%20NLP/Course%203%20-%20Week%202%20-%20Lesson%202.ipynb

# Execute this without local (lightweight Mobile) GPU (faster on CPU)
# export CUDA_VISIBLE_DEVICES= ;  python ./NLP_introduction_Laurence_Moronay/classifier.py

import jsonlines

sentences = []
labels = []

with jsonlines.open("./data/Sarcasm_Headlines_Dataset.json", "r") as datastore:
  for item in datastore:
    sentences.append(item['headline'])
    labels.append(item['is_sarcastic'])


print(len(sentences))

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

TRAINING_SIZE = 20000

training_sentences = sentences[0:TRAINING_SIZE]
testing_sentences = sentences[TRAINING_SIZE:]
training_labels = labels[0:TRAINING_SIZE]
testing_labels = labels[TRAINING_SIZE:]

VOCAB_SIZE = 10000 # all stored, but only this amount of "top" words used

tokenizer = Tokenizer(num_words=VOCAB_SIZE, oov_token="<OOV>")
tokenizer.fit_on_texts(training_sentences)
word_index = tokenizer.word_index
print(len(word_index))

sorted_by_word_count = sorted(tokenizer.word_counts.items(), key=lambda kv: kv[1], reverse=True)
words_in_vocab = {word:word_index[word] for word, index in sorted_by_word_count[0:VOCAB_SIZE]}

# only do this for small vocab_size (top are 'to', 'of', 'the', 'in', 'for', 'a', 'on', 'and', 'with', 'is')
# print(words_in_vocab)

PADDING_TYPE = "post"
TRUNC_TYPE = "post"
MAX_LENGTH = 50

training_sequences = tokenizer.texts_to_sequences(training_sentences)
training_padded = pad_sequences(training_sequences, padding=PADDING_TYPE, maxlen=MAX_LENGTH, truncating=TRUNC_TYPE)
print(training_padded.shape)

testing_sequences  = tokenizer.texts_to_sequences(testing_sentences)
testing_padded  = pad_sequences(testing_sequences , padding=PADDING_TYPE, maxlen=MAX_LENGTH, truncating=TRUNC_TYPE)
print(testing_padded.shape)
print(testing_padded[0:3])

# Need this block to get it to work with TensorFlow 2.x
import numpy as np
training_padded = np.array(training_padded)
training_labels = np.array(training_labels)
testing_padded = np.array(testing_padded)
testing_labels = np.array(testing_labels)

# Calculate the Word Embeddings
# Different dimensions thay may indicate sarcasm or not
EMBEDDING_DIMENSIONS = 16

model = tf.keras.Sequential([
  tf.keras.layers.Embedding(VOCAB_SIZE, EMBEDDING_DIMENSIONS, input_length=MAX_LENGTH),
  tf.keras.layers.GlobalAveragePooling1D(),
  tf.keras.layers.Dense(24, activation='relu'),
  tf.keras.layers.Dense(1, activation='sigmoid')
])
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())

# Fit the model
NUM_EPOCHS = 5
history = model.fit(training_padded, training_labels, epochs=NUM_EPOCHS, validation_data=(testing_padded, testing_labels), verbose=2)

# Plot the history
import matplotlib.pyplot as plt

def plot_graphs(history, string):
  plt.close()
  plt.plot(history.history[string])
  plt.plot(history.history['val_'+string])
  plt.xlabel("Epochs")
  plt.ylabel(string)
  plt.legend([string, 'val_'+string])
  # show if interactive, e.g. in a Notebook
  # plt.show()
  # Save to disk when running as pure Python
  # E.g. https://chartio.com/resources/tutorials/how-to-save-a-plot-to-a-file-using-matplotlib/
  plt.savefig(f"history_{string}.png")

plot_graphs(history, "accuracy")
plot_graphs(history, "loss")

# Define a decode_sentence function and test it
reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])

def decode_sentence(text):
    return ' '.join([reverse_word_index.get(i, '?') for i in text])

print(decode_sentence(training_padded[0]))
print(training_sentences[0])
print(labels[0])

# Save some weights, e.g. the embedding weights
embedding = model.layers[0]
weights = embedding.get_weights()[0]
print(weights.shape) # shape: (VOCAB_SIZE, EMBEDDING_DIM)

import io

out_v = io.open('./data/embedding_vecs.tsv', 'w', encoding='utf-8')
out_m = io.open('./data/embedding_meta.tsv', 'w', encoding='utf-8')
for word_num in range(1, VOCAB_SIZE):
  word = reverse_word_index[word_num]
  embeddings = weights[word_num]
  out_m.write(word + "\n")
  out_v.write('\t'.join([str(x) for x in embeddings]) + "\n")
out_v.close()
out_m.close()

# Make a sarcastic prediction ... this is clearly a bag of words
sentence = ["Yeah, that's great fun we have there, grin.", "fun grin. great that's have we there Yeah,"]
sequences = tokenizer.texts_to_sequences(sentence)
padded = pad_sequences(sequences, maxlen=MAX_LENGTH, padding=PADDING_TYPE, truncating=TRUNC_TYPE)
print(model.predict(padded))

# Same prediction, in different order => since embeddings are summed == bag of words
# [[0.62498426]
#  [0.62498426]]
