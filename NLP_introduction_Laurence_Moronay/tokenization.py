import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

sentences = [
    "Ik word warm.",
    "Ik word wakker.",
    "Word ik wakker?",
    "Jij wordt een fietser.",
    "Word jij ook wakker?",
    "Wordt u genoemd?",
    "Hij wordt later beroemd.",
    "Wordt hij later een loper?"
]

tokenizer = Tokenizer(num_words= 100, oov_token="<OOV>")
tokenizer.fit_on_texts(sentences)
word_index = tokenizer.word_index

test_sentences = sentences + [
    "Ik word nieuwsgierig.",
    "Ik wordt helemaal fout!",
    "Wordt jij ook zo fout?"
]

sequences = tokenizer.texts_to_sequences(test_sentences)

padded = pad_sequences(sequences)
print(word_index)
print(sequences, padding='post')
print(padded)
