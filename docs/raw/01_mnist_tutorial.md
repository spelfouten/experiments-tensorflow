# mnist tutorial for TensorFlow 2.x

Be careful, many of the older tutorials that have code like

```
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)
```

will dramatically fail in TensorFlow 2.x, because the examples
do not have the mnist data in the library.

Use e.g. the tutorial from the TensorFlow 2.x site:

https://www.tensorflow.org/tutorials/quickstart/beginner

Since I was able to install the cuda libraries (10.2 and
some hacking to fake 10.1 for libcudart), see
https://gitlab.com/spelfouten/pytorch-transformers/-/blob/master/docs/raw/02_trying_tensorflow_classes.md
this Tensorflow code 2.2.x uses the built in nvidia GPU of my laptop.

The output of this run with GPU is:

```
60000
10000
2020-05-17 22:22:46.792196: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcuda.so.1
2020-05-17 22:22:46.799597: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:981] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-05-17 22:22:46.799988: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1561] Found device 0 with properties: 
pciBusID: 0000:01:00.0 name: Quadro M1200 computeCapability: 5.0
coreClock: 1.148GHz coreCount: 5 deviceMemorySize: 3.95GiB deviceMemoryBandwidth: 74.65GiB/s
2020-05-17 22:22:46.800287: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudart.so.10.1
2020-05-17 22:22:46.801666: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcublas.so.10
2020-05-17 22:22:46.803300: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcufft.so.10
2020-05-17 22:22:46.803761: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcurand.so.10
2020-05-17 22:22:46.805695: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusolver.so.10
2020-05-17 22:22:46.806588: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusparse.so.10
2020-05-17 22:22:46.810108: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudnn.so.7
2020-05-17 22:22:46.810293: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:981] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-05-17 22:22:46.810649: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:981] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-05-17 22:22:46.811059: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1703] Adding visible gpu devices: 0
2020-05-17 22:22:46.811563: I tensorflow/core/platform/cpu_feature_guard.cc:143] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2020-05-17 22:22:46.836839: I tensorflow/core/platform/profile_utils/cpu_utils.cc:102] CPU Frequency: 2899885000 Hz
2020-05-17 22:22:46.837421: I tensorflow/compiler/xla/service/service.cc:168] XLA service 0x7fc140000b60 initialized for platform Host (this does not guarantee that XLA will be used). Devices:
2020-05-17 22:22:46.837440: I tensorflow/compiler/xla/service/service.cc:176]   StreamExecutor device (0): Host, Default Version
2020-05-17 22:22:46.901422: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:981] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-05-17 22:22:46.901714: I tensorflow/compiler/xla/service/service.cc:168] XLA service 0x5aae4f0 initialized for platform CUDA (this does not guarantee that XLA will be used). Devices:
2020-05-17 22:22:46.901744: I tensorflow/compiler/xla/service/service.cc:176]   StreamExecutor device (0): Quadro M1200, Compute Capability 5.0
2020-05-17 22:22:46.901958: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:981] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-05-17 22:22:46.902236: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1561] Found device 0 with properties: 
pciBusID: 0000:01:00.0 name: Quadro M1200 computeCapability: 5.0
coreClock: 1.148GHz coreCount: 5 deviceMemorySize: 3.95GiB deviceMemoryBandwidth: 74.65GiB/s
2020-05-17 22:22:46.902268: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudart.so.10.1
2020-05-17 22:22:46.902279: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcublas.so.10
2020-05-17 22:22:46.902309: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcufft.so.10
2020-05-17 22:22:46.902322: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcurand.so.10
2020-05-17 22:22:46.902369: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusolver.so.10
2020-05-17 22:22:46.902406: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusparse.so.10
2020-05-17 22:22:46.902442: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudnn.so.7
2020-05-17 22:22:46.902553: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:981] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-05-17 22:22:46.902869: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:981] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-05-17 22:22:46.903166: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1703] Adding visible gpu devices: 0
2020-05-17 22:22:46.903227: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudart.so.10.1
2020-05-17 22:22:46.903729: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1102] Device interconnect StreamExecutor with strength 1 edge matrix:
2020-05-17 22:22:46.903764: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1108]      0 
2020-05-17 22:22:46.903775: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1121] 0:   N 
2020-05-17 22:22:46.903929: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:981] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-05-17 22:22:46.904300: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:981] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-05-17 22:22:46.904657: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1247] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 2002 MB memory) -> physical GPU (device: 0, name: Quadro M1200, pci bus id: 0000:01:00.0, compute capability: 5.0)
2020-05-17 22:23:52.341755: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcublas.so.10
[[-0.27755603 -0.31710327 -0.37178338 -0.15280461 -0.02486791  0.36867163
   0.13809769  0.11858365 -0.55990845  0.4058278 ]]
[[0.07730371 0.07430621 0.07035223 0.0875748  0.0995271  0.14752086
  0.11714302 0.11487924 0.0582876  0.15310526]]
1.9137857
2020-05-17 22:23:52.585530: W tensorflow/core/framework/cpu_allocator_impl.cc:81] Allocation of 188160000 exceeds 10% of free system memory.
Epoch 1/5
1875/1875 [==============================] - 4s 2ms/step - loss: 0.2951 - accuracy: 0.9143
Epoch 2/5
1875/1875 [==============================] - 3s 2ms/step - loss: 0.1431 - accuracy: 0.9572
Epoch 3/5
1875/1875 [==============================] - 3s 2ms/step - loss: 0.1060 - accuracy: 0.9681
Epoch 4/5
1875/1875 [==============================] - 3s 2ms/step - loss: 0.0861 - accuracy: 0.9740
Epoch 5/5
1875/1875 [==============================] - 3s 2ms/step - loss: 0.0743 - accuracy: 0.9764
313/313 - 0s - loss: 0.0738 - accuracy: 0.9780
[0.07375770807266235, 0.9779999852180481]
tf.Tensor(
[[7.25792537e-09 1.99091374e-08 1.21176561e-06 7.20534736e-05
  1.09598934e-10 4.31399059e-08 3.97179197e-13 9.99919057e-01
  1.48226349e-07 7.52491724e-06]
 [3.17045519e-08 7.20964235e-05 9.99921799e-01 3.22940286e-06
  1.04845386e-13 2.08851993e-08 1.47452706e-09 5.81488118e-14
  2.85296210e-06 4.95818360e-12]], shape=(2, 10), dtype=float32)
2020-05-17 22:26:00.439225: W tensorflow/python/util/util.cc:329] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.
DONE
```

## Running WITHOUT GPU

In this SO question https://github.com/tensorflow/tensorflow/issues/2175,
I found the hint to use `export CUDA_VISIBLE_DEVICES=` to NOT use the GPU.

With this experimental code, that seems confirmed:

```
gpus = tf.config.experimental.list_physical_devices('GPU')
print(gpus)
cpus = tf.config.experimental.list_physical_devices('CPU')
print(cpus)

logical_gpus = tf.config.experimental.list_logical_devices('GPU')
print(logical_gpus)
logical_cpus = tf.config.experimental.list_logical_devices('CPU')
print(logical_cpus)
```

But ... we see similar speed. But  the CPU load is higher (above 220 %).
With the GPU activated, the CPU sticks at 100 % and the GPU works harder.

```
[]  ### no GPU visible here
[LogicalDevice(name='/device:CPU:0', device_type='CPU')]
60000
10000
...
2020-05-17 23:10:33.378192: W tensorflow/core/framework/cpu_allocator_impl.cc:81] Allocation of 188160000 exceeds 10% of free system memory.
Epoch 1/5
1875/1875 [==============================] - 2s 833us/step - loss: 0.2970 - accuracy: 0.9131
Epoch 2/5
1875/1875 [==============================] - 2s 884us/step - loss: 0.1429 - accuracy: 0.9577
Epoch 3/5
1875/1875 [==============================] - 2s 816us/step - loss: 0.1077 - accuracy: 0.9669
Epoch 4/5
1875/1875 [==============================] - 2s 820us/step - loss: 0.0866 - accuracy: 0.9736
Epoch 5/5
1875/1875 [==============================] - 2s 890us/step - loss: 0.0730 - accuracy: 0.9771
313/313 - 0s - loss: 0.0767 - accuracy: 0.9771
[0.07665000855922699, 0.9771000146865845]
```
