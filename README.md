# Experiments with tensorflow

Start some experiments with tensorflow 2.2. Looking at this Quick Start first:
https://www.tensorflow.org/tutorials/quickstart/beginner

Will this use the GPU via cuda on my laptop ??

=> Yes it does, but it seems _slower_ for this configuration than multiple cores on CPU ...
   and it takes a much longer time to start-up ...

Quicker result for small experiments with

`export CUDA_VISIBLE_DEVICES=`

## Installing the Jupyter Notebook for vsCode

Creating a features.ipynb file, but the kernel was not running and not installed.

Error was approximately: `Jupyter not installed ...`
```

Resolved by accepting the offer to install Jupyter:

```
 /usr/bin/python /home/peter_v/.vscode/extensions/ms-python.python-2020.5.80290/pythonFiles/pyvsc-run-isolated.py /home/peter_v/.vscode/extensions/ms-python.python-2020.5.80290/pythonFiles/shell_exec.py /usr/bin/python /home/peter_v/.vscode/extensions/ms-python.python-2020.5.80290/pythonFiles/pyvsc-run-isolated.py pip install -U notebook --user /tmp/tmp-9746ZAyfD4TNk62U.log
Executing command in shell >> /usr/bin/python /home/peter_v/.vscode/extensions/ms-python.python-2020.5.80290/pythonFiles/pyvsc-run-isolated.py pip install -U notebook --user
```

Now the Jupyter server is running `local`
