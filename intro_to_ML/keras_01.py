# From an example on https://www.tensorflow.org/tutorials/
# by https://twitter.com/lmoroney

import numpy as np
import tensorflow as tf

xs = np.array([-10, -1,  0, 1, 2, 3, 4, 10], dtype = float)
ys = np.array([-21, -3, -1, 1, 3, 5, 7, 19], dtype = float)

print(xs)
print(ys)

model = tf.keras.Sequential([tf.keras.layers.Dense(units=1, input_shape=[1])])
print(model.get_config())

print(model.predict([10.0]))
print(model.get_weights())

model.compile(optimizer='sgd', loss='mean_squared_error')
model.fit(xs, ys, epochs=100)
print(model.predict([10.0]))
print(model.get_weights())
