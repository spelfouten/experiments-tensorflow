import numpy as np
import tensorflow as tf


logits = [2.0, 1.0, 0.1]
exps = [np.exp(l) for l in logits]
print(exps)

softmax = exps / sum(exps)
print(softmax)

fn = tf.keras.layers.Softmax()
print(fn)
print (fn(logits))
