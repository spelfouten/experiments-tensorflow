# mnist example from Tensorflow 2 repo
# Ref. https://www.tensorflow.org/tutorials/quickstart/beginner

import tensorflow as tf

# in the terminal, set `export CUDA_VISIBLE_DEVICES=` to run with CPU only
gpus = tf.config.experimental.list_physical_devices('GPU')
print(gpus)
cpus = tf.config.experimental.list_physical_devices('CPU')
print(cpus)

logical_gpus = tf.config.experimental.list_logical_devices('GPU')
print(logical_gpus)
logical_cpus = tf.config.experimental.list_logical_devices('CPU')
print(logical_cpus)

# Load mnist dataset from Keras
mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

print(len(x_train))
print(len(x_test))

# Build the model
model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10)
])

predictions = model(x_train[:1]).numpy()
print(predictions)

# Softmax probabilities
print(tf.nn.softmax(predictions).numpy())

# Loss function
loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

# Untrained model, loss ~= 2.3 (-log(1/10))
print(loss_fn(y_train[:1], predictions).numpy())

# Compile and fit the model
model.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])
model.fit(x_train, y_train, epochs=5)

# Model performance ?
performance = model.evaluate(x_test, y_test, verbose=2)
print(performance)

# What is the probability per test image ?
probability_model = tf.keras.Sequential([
  model,
  tf.keras.layers.Softmax()
])

print(probability_model(x_test[:2]))

print("DONE")
